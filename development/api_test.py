#!/usr/bin/python

import requests
import sys

if len(sys.argv) < 2:
    print('The file name to upload must be specified')
    sys.exit(1)

host = 'http://37.17.173.56:8000'
link = '%s/items/' % host
print('\n===Creating item=== (%s)' % link)
r = requests.post(link, {'name': 'Fajl neve'}, files={'file_content': open(sys.argv[1], 'rb').read()})
print(r.status_code)
print(r.json())
_id = r.json()['id']

link = '%s/items/%s' % (host, _id)
print('\n===Receiving item=== (%s)' % link)
r = requests.get(link)
print(r.status_code)
print(r.json())

link = '%s/items/%s/' % (host, _id)
print('\n===Updating item=== (%s)' % link)
r = requests.put(link, {'name': 'Fajl uj neve'})
print(r.status_code)
print(r.json())

link = '%s/items/%s' % (host, _id)
print('\n===Receiving item=== (%s)' % link)
r = requests.get(link)
print(r.status_code)
print(r.json())

link = '%s/items/%s/' % (host, _id)
print('\n===Deleting item=== (%s)' % link)
r = requests.delete(link)
print(r.status_code)
#print(r.json())

link = '%s/items/%s' % (host, _id)
print('\n===Receiving item=== (%s)' % link)
r = requests.get(link)
print(r.status_code)
print(r.json())
