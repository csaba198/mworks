#!/bin/bash

ROOT=$1
OBS="$ROOT/obs"
API="$ROOT/api"
lst=("$OBS/models.py" "$OBS/serializers.py" "$OBS/views.py" "$OBS/urls.py")
lst+=("$API/views.py")

comm="pylint --load-plugins pylint_django --django-settings-module=mworks.settings"

rate=0
c=0
for f in ${lst[*]}; do
	printf "%-35s" "$f"
	res=`$comm $f | tail -n 2 | head -n 1`
	echo $res
	line_rate=`echo $res | awk '{print  $7}' | cut -f 1 -d\/`
	rate=`perl -e "print $rate+$line_rate"`
	c=$(($c+1))
#	echo $res | awk '{ print $7 }' | cut -f 1 -d\/
done
perl -e "print $rate/$c"
