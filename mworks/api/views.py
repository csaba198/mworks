"""
Views for the API app.
Basically his file contains the tiny API implementation.
"""
from rest_framework import viewsets

from obs.models import Item
from obs.serializers import ItemSerializer, ItemCreateSerializer, \
        ItemUpdateSerializer



# Create your views here.

class ItemViewSet(viewsets.ModelViewSet):
    """
    # As the HF description is very general and succint,
    # my API definition is also very general
    """
    queryset = Item.objects.all()


    def get_serializer_class(self):
        # In case of POST I'd like to use a different Serializer
        if self.request.method == 'POST':
            return ItemCreateSerializer
        if self.request.method == 'PUT':
            return ItemUpdateSerializer
        return ItemSerializer
