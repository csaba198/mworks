"""Model definitions for OBS"""
import random
import os

from django.db import models

from django.conf import settings
from django.urls import reverse



# Create your models here.

class Node(models.Model):
    """
    # This is a class for Nodes. If a file gets uploaded by the user via the
    # API, it'll be automatically distributed to different nodes. To how many,
    # it comes from the OBS_NUMBER_OF_NODES_TO_BE_USED variable.
    #
    # It contains only a Name and an IP field, however the network protocol (
    # so how to access the nodes, drbd, nfs, ssh, etc) is unknown, so at
    # the moment i use only directories for simulation and demonstration.
    """
    name = models.CharField(max_length=32)
    ip = models.GenericIPAddressField()



class Item(models.Model):
    """
    # A file is stored somewhere on the disk(s) and this Item class contains
    # some Meta data about it.
    #
    # * Node has an M2M relationship to Item, and it represent the physical
    # location
    # * Name is the original filename
    # * Path is basically just a placeholder. It just shows the storage path
    # on the nodes.
    # * size is the size of the uploaded binary.
    """
    node = models.ManyToManyField(Node)
    name = models.CharField(max_length=256)
    path = models.CharField(max_length=256)
    size = models.IntegerField()
    # Other attributes may come here, like where does it belong to, user object,
    # etc... .
    last_updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


    def save(self, *args, **kwargs):
        """
        #This logic could be placed also in the ItemCreateSerializer/create
        #method
        """
        newly_created = False
        if not self.pk:
            newly_created = True
        super().save(*args, **kwargs)
        # Instead of using simply .order_by('?')[:NUM] I use random.sample,
        # because i want the program fail if there're not enough nodes
        # in the database
        # The section below creates the Nodes for the Item (only if the save
        # method is called at creation)
        if newly_created:
            for node in random.sample(list(Node.objects.filter()), \
                    settings.OBS_NUMBER_OF_NODES_TO_BE_USED):
                self.node.add(node)


    def get_path_for_node(self, node):
        """
        # This method returns the path based on the node parameter
        # At the moment this is only a directory based path
        """
        return os.path.join(settings.OBS_DIR, node.name, self.path)


    def get_path(self):
        """
        # This return a random path where from the file is accessible
        # This method is called also by the download_document view
        """
        if self.node.exists():
            return self.get_path_for_node(self.node.all().order_by('?')[0])
        return ''


    def link(self):
        """
        #From this link the file can be downloaded.
        #It'll be served from different node
        #This is not an FQDN.
        """
        return reverse('download_document', args=(self.id,))


    def delete(self, *args, **kwargs):
        """If the delete method is called, we remove all the belonging
        files. If the file can't be deleted (no rights, not there, etc)
        it raises an exception. Something went wrong, needs to be checked."""
        for node in self.node.all():
            os.unlink(self.get_path_for_node(node))
        super().delete(*args, **kwargs)


    def __str__(self):
        return '%s -> %s' % (self.name[:32], \
                self.nodes.all().values_list('name', flat=True))
