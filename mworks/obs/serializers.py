"""
Serializers for the API / Item
"""
import os

from django.conf import settings
from rest_framework import serializers

from obs.models import Item
from obs.utils import get_clean_name



class ItemSerializer(serializers.HyperlinkedModelSerializer):
    """This the Serializer for GET and PUT Methods"""
    class Meta:
        model = Item
        fields = ['id', 'url', 'name', 'path', 'size', 'link', 'node', \
                'last_updated', 'created']

    link = serializers.SerializerMethodField()
    node = serializers.SerializerMethodField()


    def get_link(self, obj):
        """
        # This is here only for demonstation and testing. Otherwise simply
        # return obj.link should be used, but now this is an FQDN, however
        # this way it should not be used
        """
        return 'http://%s' % os.path.join(\
                self.context['request'].META['HTTP_HOST'], \
                obj.link().lstrip('/'))


    def get_node(self, obj):
        """This helps to show the belonging nodes as a list"""
        return obj.node.all().values_list('name', flat=True)



class ItemCreateSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for the POST method"""
    class Meta:
        model = Item
        fields = ['name', 'id']


    def create(self, validated_data):
        """
        If the user uploads a file via the API the followings happen:
        0) After a minimal validation
        1) The Item object gets created with the validated data and the gets
          chosen to the Item objects
        2) The path attribute gets set for later usage
        3) The file content gets distributed between the nodes with a very
           simply solution. Basically it gets copied.
        """

        # 0)
        if 'file_content' in self.context['request'].data:
            imuf = self.context['request'].data['file_content']
        else:
            raise ValueError('File content is not defined')

        # 1)
        item = Item.objects.create(size=imuf.size, **validated_data)

        # 2)
        path = os.path.join('%s__%s' % (item.id, get_clean_name(item.name)))
        item.path = get_clean_name(item.name)
        item.save()

        # 3)
        for count, node in enumerate(item.node.all()):
            if count:
                imuf.seek(0)
            path = item.get_path_for_node(node)
            fdesc = open(path, 'wb')
            fdesc.write(imuf.read())
            fdesc.close()
        return item



class ItemUpdateSerializer(serializers.HyperlinkedModelSerializer):
    """The only one attribute which worth to update is the name of the file"""
    class Meta:
        model = Item
        fields = ['name']
