"""Urls file for the obs application"""
from django.conf.urls import url

from obs import views



urlpatterns = [
    url(r'download_document/(?P<item_id>\d+)/$', views.download_document, \
            name='download_document'),
]
