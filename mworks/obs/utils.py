import string

def get_clean_name(name):
    valid_characters = string.ascii_letters + string.digits + '_.'
    return ''.join([b if b in valid_characters else "_" for b in name])
