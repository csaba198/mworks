"""Views for OBS.
Basically it contains only a method to download a file form the browser.
"""
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from obs.models import Item



# Create your views here.

def download_document(request, item_id):
    """
    # This view returns back a file. This serves it always from a random
    # Node if there're more than one defined to the Item. #This logic is
    # defined in the item.get_path() method.
    """
    item = get_object_or_404(Item, id=item_id)
    path = item.get_path()
    download = open(path, 'rb')
    response = HttpResponse(download.read())
    response['Content-Disposition'] = 'attachment; filename=' + item.name
    return response
    